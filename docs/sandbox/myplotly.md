# Plotly

## Plotly Charts

<div id="tester" style="width:600px;height:250px;"></div>

<script>
  
  TESTER = document.getElementById('tester');

Plotly.newPlot( TESTER, [{
  
x: [1, 2, 3, 4, 5],

y: [1, 2, 4, 8, 16] }], {
  
  margin: { t: 0 } } );

</script>

## Plotly Chloropleth

### USA

<div id="myDivUSA" style="width:100%;height:500px;"></div>

<script>

d3.csv("https://raw.githubusercontent.com/plotly/datasets/master/gapminder_with_codes.csv").then((rows) => {

  function filter_and_unpack(rows, key, year) {
    return rows.filter(row => row['year'] == year).map(row => row[key]);
  }

  var frames = []
  var slider_steps = []

  var n = 11;
  var num = 1952;
  for (var i = 0; i <= n; i++) {
    var z = filter_and_unpack(rows, 'lifeExp', num)
    var locations = filter_and_unpack(rows, 'iso_alpha', num)
    frames[i] = {data: [{z: z, locations: locations, text: locations}], name: num}
    slider_steps.push ({
        label: num.toString(),
        method: "animate",
        args: [[num], {
            mode: "immediate",
            transition: {duration: 300},
            frame: {duration: 300}
          }
        ]
      })
    num = num + 5
  }

var data = [{
      type: 'choropleth',
      locationmode: 'world',
      locations: frames[0].data[0].locations,
      z: frames[0].data[0].z,
      text: frames[0].data[0].locations,
      zauto: false,
      zmin: 30,
      zmax: 90

}];
var layout = {
    title: 'World Life Expectency<br>1952 - 2007',
    geo:{
       scope: 'world',
       countrycolor: 'rgb(255, 255, 255)',
       showland: true,
       landcolor: 'rgb(217, 217, 217)',
       showlakes: true,
       lakecolor: 'rgb(255, 255, 255)',
       subunitcolor: 'rgb(255, 255, 255)',
       lonaxis: {},
       lataxis: {}
    },
    updatemenus: [{
      x: 0.1,
      y: 0,
      yanchor: "top",
      xanchor: "right",
      showactive: false,
      direction: "left",
      type: "buttons",
      pad: {"t": 87, "r": 10},
      buttons: [{
        method: "animate",
        args: [null, {
          fromcurrent: true,
          transition: {
            duration: 200,
          },
          frame: {
            duration: 500
          }
        }],
        label: "Play"
      }, {
        method: "animate",
        args: [
          [null],
          {
            mode: "immediate",
            transition: {
              duration: 0
            },
            frame: {
              duration: 0
            }
          }
        ],
        label: "Pause"
      }]
    }],
    sliders: [{
      active: 0,
      steps: slider_steps,
      x: 0.1,
      len: 0.9,
      xanchor: "left",
      y: 0,
      yanchor: "top",
      pad: {t: 50, b: 10},
      currentvalue: {
        visible: true,
        prefix: "Year:",
        xanchor: "right",
        font: {
          size: 20,
          color: "#666"
        }
      },
      transition: {
        duration: 300,
        easing: "cubic-in-out"
      }
    }]
}; // end layout

Plotly.newPlot('myDivUSA', data, layout).then(function() {
    Plotly.addFrames('myDivUSA', frames);
  });

});

</script>

### Canada

<div id="myDivCAN" style="width:100%;height:500px;"></div>

<script>

var data = [{
  
    type: 'scattergeo',

    mode: 'markers+text',

    text: [

      'Montreal', 'Toronto', 'Vancouver', 'Calgary', 'Edmonton',

        'Ottawa', 'Halifax', 'Victoria', 'Winnepeg', 'Regina'

    ],

    lon: [

        -73.57, -79.24, -123.06, -114.1, -113.28,

        -75.43, -63.57, -123.21, -97.13, -104.6

    ],

    lat: [
      
      45.5, 43.4, 49.13, 51.1, 53.34, 45.24,

        44.64, 48.25, 49.89, 50.45

    ],

    marker: {
      
        size: 7,

        color: [
          
          '#bebada', '#fdb462', '#fb8072', '#d9d9d9', '#bc80bd',

            '#b3de69', '#8dd3c7', '#80b1d3', '#fccde5', '#ffffb3'

        ],

        line: {
          
          width: 1

        }

    },

    name: 'Canadian cities',

    textposition: [
      
      'top right', 'top left', 'top center', 'bottom right', 'top right',

        'top left', 'bottom right', 'bottom left', 'top right', 'top right'

    ],

}];


var layout = {

  title: 'Canadian cities',

    font: {
      
      family: 'Droid Serif, serif',

        size: 6

    },

    titlefont: {
      
      size: 16

    },

    geo: {
      
      scope: 'north america',

        resolution: 50,

        lonaxis: {
          
          'range': [-130, -55]

        },

        lataxis: {
          
            'range': [40, 70]

        },

        showrivers: true,

        rivercolor: '#fff',

        showlakes: true,

        lakecolor: '#fff',

        showland: true,

        landcolor: '#EAEAAE',

        countrycolor: '#d3d3d3',

        countrywidth: 1.5,

        subunitcolor: '#d3d3d3'

    }

};

Plotly.newPlot('myDivCAN', data, layout);

</script> -->

## Python Plotly

```python
import plotly.express as px

df = px.data.election()
geojson = px.data.election_geojson()

fig = px.choropleth_mapbox(df, geojson=geojson, color="Bergeron",
                           locations="district", featureidkey="properties.district",
                           center={"lat": 45.5517, "lon": -73.7073},
                           mapbox_style="carto-positron", zoom=9)
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()
```