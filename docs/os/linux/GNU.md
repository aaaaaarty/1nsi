# Le projet GNU - GNU's Not Unix (CULTURE GEEK)

## Richard Stallman

Dans les années $1980$, un programmeur de système d'exploitation au **[Massachusetts Institute of Technology (MIT)]()**, un certain **[Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman)**, commence à constater des restrictions des possibilités d'utilisation des Logiciels. Il se trouve face au problème éthique de devoir développer des logiciels dont l'utilisation sera restreinte, qui ne pourront pas être partagés en raison des droits du propriétaire/acheteur du logiciel (souvent distinct du créateur). 

<figure style="width:50%;float:left;">
<img src="../img/richard_stallman_young.jpeg" class="box">
<figcaption><b>Richard Stallman, jeune</b></figcaption>
</figure>

<figure style="width:50%;float:left;">
<img src="../img/richard_stallman_now.png" class="box">
<figcaption><b>Richard Stallman, aujourd'hui</b></figcaption>
</figure>

<clear></clear>

## L'origine du problème

Stallman commença à constater ces restrictions en présence de programmes sur lesquels il ne pouvait pas intervenir, un pilote d'imprimante notamment. Il se souvient qu'un chercheur au **Xerox PARC - Palo Alto Research Center**, probablement un certain **[Robert (Bob) Sproull](https://en.wikipedia.org/wiki/Bob_Sproull)**, aurait refusé de lui fournir le code source du pilote en raison d'un contrat de non divulgation que **Xerox** avait passé avec lui, pratique encore peu courante à l'époque [^1].

Bien qu'anecdotique, cette petite histoire est souvent prise comme étant le point de départ de l'<bred>informatique libre</bred>, puisque c'est à partir de là semble-t-il que Richard Stallman consacrera son énergie à résoudre ce problème de conscience, ce qui fera de lui **le premier et le plus emblématique des ambassadeurs du** <bred>Logiciel Libre</bred> : "comme dans *Liberté*, pas comme dans *Gratuit*".

<clear></clear>

## Le Projet GNU

<div style="width:30%; float:right;">

<figure>
<img src="../img/gnu.png" class="box">
<figcaption><b>La mascotte du Projet GNU</b></figcaption>
</figure>

</div>

Ainsi naît le **[projet GNU (GNU's Not Unix)](https://fr.wikipedia.org/wiki/Projet_GNU)** [^2], développé **[Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman)** en **1983** (à 30 ans), fondateur de la **[FSF - Free Software Fondation, 1985](https://www.fsf.org/?set_language=fr)** [^3]. 

Le **projet GNU** est un ensemble de **[Logiciels Libres](https://fr.wikipedia.org/wiki/Logiciel_libre)** destinés à constituer un **Système d'Exploitation (dit GNU également)** à part entière: l'Editeur Emacs, gcc (Compilateur C), g++ (compilateur c++), gmake, .... 

## Les Systèmes d'Exploitation GNU/Linux

Le projet GNU manquait quant à lui (et pendant longtemps) d'un noyau, et était donc le complément idéal pour le **noyau Linux**. L'addition du noyau Linux et des logiciels du projet GNU, forment ce que l'on appelle quelquefois le **Système d'Exploitation GNU/Linux** ou quelquefois plus simplement (mais erronément) **Linux**.

<clear></clear>

## Le Logiciel Libre

### Qu'est-ce que le Logiciel Libre ?

Un [<bred>Logiciel Libre</bred>](https://fr.wikipedia.org/wiki/Mouvement_du_logiciel_libre) [^4] $^{,}$ [^5] désigne un logiciel qui **respecte la liberté des utilisateurs**. En gros, cela veut dire que les utilisateurs ont la liberté **d'exécuter, de copier, de distribuer, d'étudier, de modifier et d'améliorer ces logiciels**. Ainsi un logiciel libre fait référence à __"Libre, comme dans *Liberté*, pas comme dans *Gratuit*"__. Un logiciel libre *peut* donc, en théorie, être commercial.

!!! note ""
    Selon la FSF, un programme est un <bred>Logiciel Libre</bred> si, en tant qu'utilisateur de ce programme, vous avez les **$4$ libertés fondamentales** [1] :

    * **Liberté 0** : la liberté de faire fonctionner le programme comme vous voulez, pour n'importe quel usage (liberté 0) ;
    * **Liberté 1** : la liberté d'étudier le fonctionnement du programme, et de le modifier pour qu'il effectue vos tâches informatiques comme vous le souhaitez (liberté 1) ; l'accès au code source est une condition nécessaire ;
    * **Liberté 2** : la liberté de redistribuer des copies, donc d'aider les autres (liberté 2) ;
    * **Liberté 3** : la liberté de distribuer aux autres des copies de vos versions modifiées (liberté 3) ; en faisant cela, vous donnez à toute la communauté une possibilité de profiter de vos changements ; l'accès au code source est une condition nécessaire.

Aller Plus Loin [^4]

### Copyleft <i class="fa fa-copyright fa-flip-horizontal"></i>

Initié par Richard Stallman dans les années $1970$, le <bred>copyleft <i class="fa fa-copyright fa-flip-horizontal"></i></bred>, parfois traduit <bred>gauche d'auteur</bred> :fr:, (pour résumer très simplement) est l'autorisation donnée par l'auteur d'un travail soumis au droit d'auteur (oeuvre d'art, texte, programme informatique ou autre) d'utiliser, d'étudier, de modifier et de diffuser son oeuvre, **dans la mesure où cette même autorisation reste préservée dans les travaux dérivés**. La notion de copyleft couvre des logiciels, des documents, des oeuvres artistiques, des découvertes scientifiques, et même cetains brevets...

Cette autorisation n'entre pas en conflit avec les $4$ libertés fondamentales ; en fait, elle les protège. Les Licences Copyleft <i class="fa fa-copyright fa-flip-horizontal"></i>, dont la plus célèbre, la *Licence GPL - GNU General Public License*, sont considérées **protectrices** et **réciproques**, contrairement à d'autres licences libres permissives.

Le concept de copyleft symbolise en ce sens l'esprit créatif et moqueur de la **culture hacker du MIT**, à laquelle adhère Stallman.

## Références :gb:

[^1]: [Framabook : Richard Stallman et la révolution du logiciel libre, page 9](https://framabook.org/docs/stallman/framabook6_stallman_v1_gnu-fdl.pdf)
[^2]: [gnu.org : Système d'Exploitation GNU](https://www.gnu.org/philosophy/free-sw.fr.html)
[^3]: [fsf.org : FSF - Free Software Fondation](https://www.fsf.org/?set_language=fr)
[^4]: [GNU : Qu'est-ce- que le Logiciel Libre ?](https://www.gnu.org/philosophy/free-sw.fr.html)
[^5]: [Wikipedia : le Mouvement du Logiciel Libre](https://fr.wikipedia.org/wiki/Mouvement_du_logiciel_libre)
