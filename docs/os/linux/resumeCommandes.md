# 1NSI : Résumé de Commandes Linux de base

<center>

| Commande | Signification | Exemples |
|:-:|:-:|:-:|
| <bred>Commandes de Fichiers et Dossiers</bred> |||
| `pwd` | Affiche le **chemin absolu**<br/>du Dossier courant<br/>**P**rint **W**orking **D**irectory :gb: | `pwd` |
| `cd` | Change de Répertoire<br/>**C**hange **D**irectory :gb: | `cd` ou `cd ~` revient au **répertoire personnel** `home` de l'utilisateur courant<br/>`cd -` revient vers le **répertoire précédent**<br/>`cd /` se déplace sur **la racine** `/`<br/>**chemin relatif** : `cd ..` remonte au **dossier parent**<br/>**chemin relatif** : `cd ../chemin/vers/dossier` vers dossier d'un parent <br/>**chemin relatif** : `cd chemin/vers/dossier` vers dossier d'un enfant<br/>**chemin absolu** : `cd /chemin/vers/dossier` vers dossier "absolu" |
| `ls` | Liste les fichiers/dossiers<br/>**L**i**S**t :gb: | `ls` liste (***court***)<br/>`ls -l` liste ***long***<br/>`ls -al` liste ***long TOUT/ALL : même les fichiers cachés***<br/>`ls -l cheminVers/test/` liste tout le contenu du dossier `test/`<br/> où cheminVers est un **chemin relatif** ou un **chemin absolu** |
| `mkdir` | Crée un nouveau répertoire<br/>**M**a**K**e **DIR**ectory :gb: | `mkdir monDossier` |
| `rmdir` | Supprime un répertoire **VIDE**<br/>**R**e**M**ove **DIR**ectory :gb: | `rmdir monDossier` |
| `rm` | Supprime un répertoire<br/>:warning:**ET TOUT SON CONTENU**:warning:<br/>**R**e**M**ove :gb: | :one: `rm toto.txt tata.md` Supprime les deux fichiers `toto.txt` et `tata.md`<br/> :two: `sudo rm -i test/` supprime **interactivement** (avec confirmation)<br/> :three: `sudo rm -f test/` supprime en **forçant** (sans confirmation) <br/> :four: `rm -r test` ou `rm -r test/` Supprime **récursivement** le dossier `test` et **TOUT** son contenu<br/> :five: `sudo rm -Rf /*` :warning: **DANGER** :warning:**¡¡ SUPPRIME TOUT SUR VOTRE ORDI !!** |
| `touch` | Créer un nouveau fichier **VIDE** | `touch nouveauFichier` |
| `cp` | **C**o**P**ie un fichier | `cp monFichier monFichierCopie` |
| `mv` | Déplace un fichier / **M**o**V**e :gb: | `mv monFichier /nouveau/chemin/dossier` |
| <bred>Affichage d'infos</bred> |||
| `cat` | Affiche le contenu d'un fichier | `cat monFichier`<br/>`cat monFichier1 monFichier2 > monFichier3` concatène deux fichiers<br/>et place le résultat dans le nouveau fichier `monFichier3` |
| `nano` | Éditeur de fichier dans Terminal<br/>Simple mais Efficace | `nano monFichier`, puis :<br/>`Ctrl+O` pour sauvegarder<br/>`Ctrl+X` pour sortir de `nano` |
| `vi` ou `vim` | Éditeur de fichier dans Terminal<br/>**Véritable IDE dans le Terminal**<br/> Emblématique des *Nerds*<br/>Déconseillé pour Débutants.. (?)<br/>**V**i **IM**proved :gb: | [Tutoriel `vim` sur OpenClassrooms](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/42693-vim-lediteur-de-texte-du-programmeur) |
| `more` | ***pager***: Affiche un fichier **page par page** <br/>`z` page avant, `w` page arrière<br/>`q` quit, `h` help| `more monFichier` |
| `less` | ***pager*** = `more` + commandes `vi` :<br/>Affiche un fichier, page par page<br/> | `less -r monFichier`<br/> `ps -aux | less` affiche tous les processus, **page par page** |
| `id` | Affiche des infos utilisateur<br/>`uid`, `gid` et `groupes` | `id eleve` |
| `free` | Affiche la mémoire disponible/utilisée<br/>par le système | `free -t` |
| `ps` | Affiche une photo/snap des processus | `ps -aux`<br/> `ps -aux | grep gimp` |
| `pstree` | Affiche une arborescence des processus | `pstree -p` |
| `top` | Affiche la charge du CPU | `top` |
| <bred>Recherche</bred> |||
| `grep` | Recherche une chaîne de caractères<br/>(et même des ***motifs*** de chaînes)<br/>(ou des ***expressions régulières,*** <br/> **"regex"**) dans un fichier | `grep maChaine monFichier`<br/>`grep motif/regexp monFichier` <br/>`grep -i motif/regexp monFichier` **insensible** à la casse<br/>`grep -c motif/regexp monFichier` en **comptant** les occurences<br/>`grep -v motif/regexp monFichier` renverse recherche: **tout sauf** le motif<br/>`grep -n motif/regexp monFichier` ajoute les **numéros** de ligne |
| `find` | Recherche des fichiers/dossiers | `sudo find / -name "test.md"` |
| `locate` | Recherche des fichiers/dossiers<br/> parmi ceux indexés | `locate test.md` |
| <bred>Divers</bred> |||
| `ln` | Crée un lien symbolique<br/>vers un Dossier/Fichier<br/>**L**i**N**k :gb: | `ln -s chemin/versFichierOuDossier chemin/ouPlacerLien` <br/> Crée un **lien symbolique** (/raccourci) <br/>qui mène (via un chemin) vers un Fichier/Dossier (destination)<br/>et place le lien ici (source) : `chemin/ouPlacerLien` |
| `kill` | Envoie un signal à un processus<br/>dont on connaît le *PID* | `kill 2318`<br>`kill -s 9 2318` |

</center>






