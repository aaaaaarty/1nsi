# Systèmes d'Exploitation - OS

## Qu'est-ce q'un Système d'Exploitation ?

Programmer en **assembleur** est chose difficile, c'est pourtant le langage compris par la machine. Il est donc intéressant de disposer d'un intermédiaire entre l'Homme et la Machine. C'est le rôle principal d'un <bred>Système d'Exploitation</bred> (SE) ou <bred>Operating System (OS)</bred> :gb: : c'est lui qui en charge de *"traduire"* ce que souhaite faire un utilisateur Humain en **Langage machine** afin que l'ordinateur comprenne et exécute l'action.

## Fonctions d'un Système d'Exploitation

Le Système d'Exploitation (OS) est donc en charge de:

* Fournir une interface entre l'Humain et la Machine
* Gérer les ressources de l'ordinateur, à savoir :
    * sa mémoire
    * son processeur
    * ses périphériques (écrans, imprimantes, etc..)
    * la gestion de l'énergie
    * etc..
* Gérer les utilisateurs ainsi que leurs droits d'accès à certains fichiers
* Assurer l'indépendance vis à vis du matériel (un même OS peut être exécuté sur des ordinateurs différents, n'ayant pas les mêmes composants matériels)
* Rendre concret ce qui ne l'est pas (par ex. un fichier est un concept à priori abstrait, mais l'OS le rend concret)

