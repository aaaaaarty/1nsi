# 1NSI : Représentation des Nombres en Base 16 (Hexadécimal)

Dans toute cette partie, les nombres considérés sont des **nombres entiers positifs**.

## Notation Hexadécimale

!!! def "Notation Hexadéximale d'un Entier"

    * En base $16$, on doit utiliser $16$ chiffres en tout : 
        * on reprend les $10$ chiffres de $0$ à $9$ que l'on utilisait déjà en base $10$, puis 
        * on ajoute les $6$ nouveaux ***chiffres*** suivants: $A$, $B$, $C$, $D$, $E$, $F$
    * Comme en base $2$ et $10$, l'écriture en base $16$ est positionnelle: chaque chiffre étant multiplié par une puissance de $16$.

<center>

|Chiffre en base $16$ | Valeur en base $10$ |
|:-:|:-:|
| $0$ | $0$ |
|...|...|
| $9$ | $9$ |
| A | $10$ |
| B | $11$ |
| C | $12$ |
| D | $13$ |
| E | $14$ |
| F | $15$ |

</center>

## Conversion Base 16 $\rightarrow$ Base 10

!!! exp
    $4A5E = 4\times16^3+10\times16^2+5\times16^1+14\times16^0=19038_{10}$  

**Détails:**

<center>

| Hexadécimal | $4$ | $A$ | $5$ | $E$ |
| :-: | :-: | :-: | :-: | :-: |
| Exposant | $3$ | $2$ | $1$ | $0$ |
| Puissance de $16$ | $16^3$ | $16^2$ | $16^1$ | $16^0$ |

</center>

!!! python
    ```python
    >>>int('0x3c2e',16)
    15406
    ```

## Conversion Base 10 $\rightarrow$ Base 16

![Conversion Décimale -> Hexadécimal](../img/ConversionDecimale2Hex.png){.center .box width=40%}

On en déduit que: $2526_{10}=(9,13,14)_{16}=9DE_{16}$

!!! python
    ```python
    >>>hex(1534)
    '0x5fe'
    ```