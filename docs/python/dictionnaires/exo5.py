def fusionDicos(listeDicos:list)->dict:
  d = {}
  for dico in listeDicos:
    for cle, valeur in dico.items():
      if cle not in d.keys():
        d[cle] = [valeur]
      else:
        d[cle].append(valeur)
  return d

print(fusionDicos([d1,d2,d3]))