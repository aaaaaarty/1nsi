# 1NSI : Exercices Fonctions & Arithmétique

!!! ex "est_paire()"
  1. Écrire une fonction `est_pair(n:int)->bool` qui reçoit en entrée un nombre entier `n` et qui renvoie en sortie si OUI (True) ou NON (False) le nombre `n` est pair.

!!! ex "est_entier()"
  1. Écrire une fonction `est_entier(x:float)->bool` qui reçoit en entrée un nombre flottant `x` et qui renvoie en sortie si OUI (True) ou NON (False) le nombre `x` est entier.

!!! ex "Divisibilité de a par b"

1. Écrire une fonction `div_par(a:int,b:int)->bool` qui teste la divisibilité de l'entier `a` par l'entier `b`, autrement dit: 
* Si `a` est divisible par `b`, alors la fonction renvoie `True`
* sinon, elle renvoie `False`
  
2. Écrire une fonction `diviseur(a:int,b:int)->bool` qui teste si l'entier `a` divise l'entier `b`, c'est-à-dire que `b` est un multiple de `a`, (on pourra, si le souhaite, utiliser la fonction de la question precédente `div_par(a,b)`)
  
3. En déduire une fonction `diviseurs(n:int)->list` qui accepte en entrée un entier `n`, et qui renvoie en sortie la liste de tous les entiers `p` qui sont des diviseurs de `n`
  
4. Écrire une fonction `sigma(n)` qui accepte en entrée un entier `n`, et qui renvoie en sortie la somme de tous les diviseurs de `n`
  
5. >**Définition :** Un **diviseur strict** d'un entier `n` est un diviseur de `n` qui soit différent de `1` et de `n`. 
Écrire une fonction `diviseurs_stricts(n:int)->list` qui accepte en entrée un argument entier `n`, et qui renvoie en sortie la liste de tous les diviseurs stricts de `n`
  
  
</ex>
  
<ex data="Nombres Premiers">
  
Un nombre $n$ est dit <bred>premier</bred> s'il est **seulement** divisible par $1$ et par lui-même.
  
Exemples:
* $5$ est **premier** car $5$ est divisible par $1$ et par $5$, **et c'est tout (par aucun autre nombre)**
* $6$ n'est **pas premier**, car $6=3\times 2$, ce qui prouve que $6$ est divisible :
  * par $1$ et par $6$ (lui-même), mais aussi par :
  * par $2$ et aussi par $3$...
  
1. Écrire une fonction `est_premier(n:int)->bool` qui prend en entrée un argument `n:int`, et qui renvoie en sortie :
* `True` si `n` est un nombre premier,
* `False`, sinon
2. **En déduire** une deuxième fonction `premiers(x:int)->list` qui accepte en entrée un argument `x:int`, et qui renvoie en sortie la liste de **tous les nombres premiers inférieurs ou égaux à `x`**
3. Écrire une fonction `qte_premiers(x:int)->int` qui accepte en entrée un entier `x` et qui renvoie en sortie la quantité de nombres premiers inférieurs ou égaux à `x`
4. Grâce au module **matplotlib**, tracer la courbe représentant la fonction `qte_premiers` : $x \mapsto \text{qte\_premiers()}$
On pourra commencer par créer, par compréhension de liste, les deux listes suivantes:

    ```python
    x = [50, 100, 150, ..., 1000]
    y = [qte_premiers(50), qte_premiers(100), qte_premiers(150), ..., qte_premiers(1000),]
    ```

</ex>
  
<ex data="Nombres Parfaits">
  
>**Définition :** un nombre entier `n` s'appelle un **nombre parfait** lorsque la somme de tous les diviseurs (positifs) de `n` vaut le double de `n`, autrement dit lorsque : 
><center>
>
><enc>$$\sigma(n) = 2n$$</enc>
>
></center>

où $\sigma(n)$ désigne la fonction `sigma(n)` crée à l'exercice 1, 4°), donc $\sigma(n)$ désigne la somme des diviseurs positifs de `n`

1. Écrire une fonction `parfait(n:int)->bool` qui accepte en entrée un entier `n`, et qui renvoie en sortie:
* `True` si `n` est un nombre parfait
* `False` sinon
2. Écrire une fonction `parfaits(x:int)->list` qui accepte en entrée un entier `x` et qui renvoie en sortie la liste de tous les nombres entiers parfaits inférieurs ou égaux à `x`


</ex>