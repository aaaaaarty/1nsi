# 1NSI : Exercices Fonctions

!!! ex "contient"
    Ecrire une fonction `contient_mot(mot:str, phrase:str)->bool` qui prend en entrée deux arguments `mot` et `phrase`, et qui renvoie en sortie:
      * `True` si le caractère appartient à la phrase
      * `False` sinon

!!! ex "minorite (en âge)"
    Ecrire une fonction `est_majeur(anneeNaissance:int)->bool` qui prend en entrée un seul argument `anneeNaissance:int`, et qui renvoie en sortie:
    * `True` si tu es majeur
    * `False` sinon








