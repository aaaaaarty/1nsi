# 1NSI : Structures Conditionnelles

## Opérateurs de Comparaison 1

### Test / Condition / Comparaison

Un **Opérateur de Comparaison** est la donnée de *un ou plusieurs caractères* permettant de poser une question à Python, appelée un <bred>test</bred> (de comparaison) ou une <bred>condition</bred> ou une <bred>comparaison</bred>, dont la réponse est toujours un type de données <bred>Booléen</bred>, càd :

* `True` (Vrai) ou
* `False` (Faux)

```python
>>> 2<3
True
>>> 7>9
False
```

### Liste des Opérateurs de Comparaison

<center>

| Opérateur | Signification | Exemple<br/>de test | Exemple de réponse |
|:-:|:-:|:-:|:-:|
| `<` | strictement inférieur à | `2<3`<br/>`3<1` | `True`<br/>`False` |
| `>` | strictement inférieur à | `5>3`<br>`3>1` | `True`<br/>`False` |
| `<=` | inférieur ou égal à | `4<=9`<br/>`5<=3` | `True`<br/>`False` |
| `>=` | supérieur ou égal à | `7>=1`<br/>`3>=8` | `True`<br/>`False` |
| `==` | est égal à | `2==4/2`<br/>`3==2` | `True`<br/>`False` |
| `!=` | non égal à | `2!=3`<br/>`2!=4/2` | `True`<br/>`False` |

</center>

### Opérateurs `not`, `and`, `or`

<center>

| Opérateur | Signification | Exemple<br/>de test | Exemple de réponse |
|:-:|:-:|:-:|:-:|
| `not` | non<br/>(le contraire de) | `not(2<3)` $\,\,$ `not 2<3`<br/>`not(3<1)` $\,\,$ `not 3<1` | `False`<br/>`True` |
| `and` | et | `1<2 and 3>=3`<br>`2<1 and 3==3`<br/>`4>=2 and 2==1` | `True`<br/>`False`<br/>`False` |
| `or` | ou | `1<2 or 3>=3`<br>`2<1 or 3==3`<br/>`4>=2 or 2==1`<br/>`4<2 or 2==1` | `True`<br/>`True`<br/>`True`<br/>`False` |

</center>

### Opérateur `in`

L'opérateur `in` permet de savoir le contenu situé à sa gauche **est inclus dans** le contenu situé à sa droite.

```python
>>> "au" in "bonjour Laura"
True
>>> "ar" in "bonjour Laura"
False
>>> 2 in [3,4,2,5,7.6]
True
```

## Structures Conditionnelles

### if

La structure conditionnelle `if` permet exécuter une (ou plusieurs) instruction.s SI une condition est vraie (et seulement dans ce cas là).

```python
age=int(input("T'as quel âge ? "))
if age<18:
  print("Tu es mineur")
```

Remarquer que dans ce cas, il n'y aura **aucun affichage** si l'age est supérieur (ou égal) à 18.

### if / else

La structure conditionnelle `if.. else..` permet de n'exécuter une (ou plusieurs) instruction.s, que **SI la condition du `if` est Vraie**, SINON ce sont les (autres) instructions situées après le `else` qui sont exécutées.

```python
age=int(input("T'as quel âge ? "))
if age<18:
  print("Tu es mineur")
else:
  print("Tu es majeur")
```

Dans tous les cas (quel que soit l'âge), une des deux phrase est affichée (on ne peut oublier personne)

### if / elif / else

La structure conditionnelle `if elif else` permet de réaliser plusieurs (plus de deux) tests de comparaison, afin d'affiner les réponse à réaliser

```python
age=int(input("T'as quel âge ? "))
if age<18:
  print("T'es trop jeune")
elif age>=18 and age<30: # else if <=> elif
  print("T'es jeune, t'es sympa!")
else: # dans tous les autres cas (age >= 30)
  print("T'es trop vieux!")
```