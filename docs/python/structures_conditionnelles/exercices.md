# 1NSI : Exercices Tests Conditionnels

!!! ex "Détecter le signe d'un nombre"
    Créer un script Python qui demande en entrée un nombre (flottant) `x` et qui affiche en sortie le signe de `x` :

    * Si x<0, Alors afficher "Strictement Négatif"
    * Si x=0, Alors afficher "Nul"
    * Si x>0, Alors afficher "Strictement Positif"

!!! ex "Détecter si un nombre entier est pair ou impair"
    Créer un script Python qui demande en entrée un nombre entier `x`, et qui affiche en sortie, si ce nombre est pair, ou pas (impair)

!!! ex "Vérifier si une chaîne contient une sous-chaîne de cractère"
    Créer un script Python qui :
    
    * crée une variable phrase="une phrase qui vous plaît" (on dit "**coder en dur**")
    * demande en entrée une (sous-)chaîne de caractères
    * affiche en sortie, si OUI ou NON la (sous-)chaîne appartient à la phrase initiale.


