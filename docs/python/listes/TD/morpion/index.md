# TD Morpion

On initialise la grille vide du morpion par la liste suivante:

```python
g = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]
```

On fait le choix que :

* `1` dans une case représente un croix (joueur 1)
* `2` dans une case représente une cercle (joueur 2, qui sera une IA - Intelligence Artificielle)

Remarque:
On peut intialiser les variables suivantes:

```python
X = 1
O = 2
```

1°) Créer une fonction `jouer_utilisateur()` qui 

* demande au joueur 1 la ligne et la colonne où il veut placer une croix
Exemple: le joueur1 répondra par une chaîne de caractère comme `"02"`, ce qui devra signifier que la croix est placée à la ligne 0 et la colonne 2
* met à jour la grille `g`