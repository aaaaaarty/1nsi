# 1NSI : Réseaux - Décomposition en Couches

Il s'agit ici de modéliser la manière de communiquer entre deux hôtes interconnectés (directement, ou via un réseau, voire un réseau de réseaux)

## Modèle en Couches

### Principe Général Simplifié

* Les données sont transmises au Système d'Exploitation (OS) à l'aide d'une Application (logiciel)
* L'OS les coupe en plusieurs petits <bred>paquets</bred> (multiplexage), les numérote, et les transmet à la carte réseau
* la carte réseau les envoie sur le réseau (qui peut se résumer à un simple câble, en cas de connexion directe)
* La carte réseau du destinataire reçoit les paquets transmis, et les envoie au Système d'Exploitation (OS)
* l'OS reconstitue le message et le transmet à l'application de destination

Pour être plus précis, les traitements successifs subis par les paquets de données dans un hôte sont modélisés en ce que l'on appelle des <bred>couches</bred> ou <bred>niveaux</bred> d'abstraction, indépendantes les unes des autres, mais travaillant ensemble. Chaque couche étant spécialisée dans une ou plusieurs opérations et protocoles spécifiques. Cette modélisation en couches est valable, aussi bien :

* pour l'envoi de données depuis l'hôte source), que 
* pour la réception de données (par l'hôte destination)

### Le Principe de Décomposition en Couches

![Modèle en Couches](../img/modele-en-couches.svg){.center}

Pour faire communiquer des ordinateurs interconnectés (soit directement, soit via un ou plusieurs réseaux), les informations sont d'abord traduites sous forme de plusieurs couches :

* **Chaque couche $i$ (inférieure) d'un hôte rend un ou des <bred>services</bred> à la couche $i+1$ directement supérieure** du même hôte
* **Chaque couche $i$ (supérieure) d'un hôte utilise les <bred>services</bred> de la couche $i-1$ directement inférieure** du même hôte. 
* Chaque couche $i$ inférieure est une abstraction pour masquer la complexité à la couche $i+1$ supérieure ($1$ couche $=1$ boîte noire)
* Chaque couche du modèle résout une partie des problèmes de communication
* Chaque couche $i$ d'un hôte communique avec la même couche $i$ correspondante sur l'hôte interconnecté, en utilisant un ou plusieurs <bred>protocoles</bred> **spécifiques à cette couche $i$**. 
* Pour chaque communication depuis une source vers une destination: les données descendent toutes les couches de l'hôte source une par une (des supérieures vers les inférieures), puis les remontent dans l'hôte destination dans le sens contraire (des inférieures vers les supérieures). Ce processus (de descente puis remontée des couches) reste valable lorsqu'une couche de l'hôte source discute avec la couche équivalente de l'hôte destination.
* Chaque protocole de communication est associé à une couche spécifique (uniquement)

!!! exp "Une image de la Communication entre Couches"
    Soient 2 explorateurs français, l’un en Espagne, l’autre au Pérou.

    * Ils ne peuvent parler directement car ils ne savent pas utiliser le télégraphe.
    $\rightarrow$ canal de communication
    * Ils ont besoin d’un technicien pour envoyer et recevoir les infos.   
    $\rightarrow$ couche de niveau $1$
    * Ils ne parlent pas espagnol, ils ont besoin d’un interprète.  
    $\rightarrow$ couche de niveau $2$

### Avantages

* Si une couche est défaillante, les autres couches ne cessent pas de fonctionner, et peut donc s'appuyer sur les couches inférieures
* Le débogage est simplfié dans un modèle en couches

## Deux Modèles: OSI vs TCP/IP

Il existe principalement deux modèles en couches:

* Le <bred>modèle OSI - Open Systems Interconnection</bred>, est une **norme ISO - International Standards Organisation** ou Organisation Internationale de Normalisation. C'est un **modèle Académique et Abstrait** (sans implémentation pratique), mais qui admet néanmoins une utilité réelle. **Le modèle OSI contient $7$ couches**.
* Le <bred>modèle TCP/IP</bred> est une **implémentation pratique**, et un standard qui s'est imposé de fait. c'est ce modèle qui est utilisé en pratique. **Le modèle TCP/IP contient $4$ couches**.

### Communication par couches, via connexion directe

<center>

<div>

<div style="width:48%; float:left;">

<figure>
<img src="../img/osi-deux-ordis.svg">
<figcaption>

Décomposition en Couches,<br/>
<bred>Modèle OSI</bred>,<br/>
Connection directe entre deux PC

</figcaption>
</figure>

</div>

<div style="width:48%; float:right;">

<figure>
<img src="../img/tcpip-deux-ordis.svg" >
<figcaption>

Décomposition en Couches,<br/>
<bred>Modèle TCP/IP</bred>,<br/>
Connection directe entre deux PC

</figcaption>
</figure>

</div>

</div>

</center>

<clear></clear>


### Communication par Couches, via un inter-Réseau

![OSI passage par Switch Routeur](../img/osi-switch-routeur.svg){.center}

<figure> 

<figcaption>

SW1=Switch Numéro 1, SW2 = Switch Numéro 2<br/>
R1 = Routeur Numéro 1

</figcaption>

</figure>

### Rôle de chaque couche 

<center>

<figure>

<img src="../img/OSI_vs_TCP_IP.png" class="box">

<figcaption>

Rôle des couches Modèle OSI vs Modèle TCP/IP

</figcaption>

</figure>

</center>

<env>Le cas de TCP/IP</env>

* La couche <bred>Application</bred> du modèle TCP/IP recouvre en fait les $3$ couches supérieures du modèle OSI (Les couches Application, Présentation, Session). Ce n'est pas qu'elles n'existent pas dans TCP/IP. Le modèle OSI est juste plus détaillé que le modèle TCP/IP quant à ses couches.
* La couche <bred>Transport</bred> de TCP/IP est quelquefois appelée la <bred>couche TCP - UDP</bred> pour **TCP = Transmission Control Protocol** et/ou **UDP= User Datagram Protocol**
* La couche <bred>Réseau</bred> de TCP/IP est souvent appelée la <bred>couche IP - Internet Protocol</bred>
* La Couche <bred>Accès Réseau</bred> de TCP/IP est souvent divisée en deux: 
    * Couche Liaison de Données
    * Couche Physique 


## notes et Références

[^1]: [Outils de Couche Réseau](https://www.it-connect.fr/chapitres/les-outils-de-couche-reseau/)

