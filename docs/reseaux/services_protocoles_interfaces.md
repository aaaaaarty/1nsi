# 1NSI : Services, Protocoles & Interfaces

## Services

!!! def "Service"
    Un <bred>Service</bred> (réseau) est une description abstraite de fonctionnalités à l'aide de *primitives* (commandes ou événements) telles que par exemple: demande de connexion ou réception de données.

## Protocoles

!!! def "Protocole"
    Un <bred>Protocole</bred> de communication réseau est un ensemble formel de règles qui rendent les communications possibles entre hôtes interconnectés. Les unités sont censées les respecter.  
    Les protocoles définissent une sorte de langage/procédure commun(e) que les unités utilisent pour se trouver, se connecter l’une à l’autre et y transporter des informations.

### Protocole Fiable & Acquittement

!!! def "Protocole Fiable"
    Dans les réseaux informatiques:

    * Un <bred>protocole fiable</bred> ou <bred>reliable protocol</bred> :gb: fournit l'assurance de la bonne livraison des données au destinataire(s) par opposition à 
    * un <bred>protocole peu fiable</bred> ou <bred>unreliable protocol</bred>, qui ne fournit aucune garantie (pas de notifications à l'expéditeur concernant la livraison des données transmises).

Forcément, les protocoles fiables impliquent généralement davantage de ressources à l'implémentation (donc plus lents) que les protocoles non fiables (plus rapides),

!!! exp "de Protocoles Fiables ou Peu fiables (à connaître)"
    * Le protocole TCP, de niveau $4$ (la couche Transport du modèle TCP/IP), est fiable
    * Les protocoles fiables sont utilisés lorsque 
        * conserver la totalité de l'information est important: par ex. les emails et le protocole SMTP qui utilise TCP..., etc...
        * quitte à perdre en rapidité
    * le Protocole UDP est peu fiable
    * Les protocoles UDP sont en général utilisés lorsque:
      * perdre une (petite) partie de l'information n'est pas considéré comme grave (par ex. Jeux Vidéos, où l'on peut supporter de perdre une frame)
      * mais la vitesse est importante

!!! pte "Ack = Acquittement"
    Dans les protocoles fiables, les récepteurs informent les émetteurs de :
    
    * la **bonne réception** des données par un processus nommé **Acquittement**, et nommé **Ack** (pour **Acknowlegment** :gb:)
    * la **mauvaise réception** par un **acquittement négatif**, via la flag **NAck**
    * Il en existe d'autres, et la terminologie exacte dépend ensuite du protocole

### Protocole avec/orienté Connexion

!!! def "Protocole orienté connexion"
    Un <bred>protocole (réseau) orienté connexion</bred> ou <bred>protocole avec connexion</bred> est un protocole qui livre un **flux de données** dans le même ordre que celui dans lequel il a été envoyé, après avoir d'abord établi une **session de communication** (avec authentification).

!!! exp "Protocoles avec ou sans connexion"
    * Le protocole TCP est avec/orienté connexion
    * Le protocole IP est sans connexion
    * Les protocoles orientés connexion supportent beaucoup plus efficacement le trafic temps réel (VoIP, Visio-conférences, etc..) que les protocoles sans connexion

!!! pte "Fiable vs Orienté Connexion"
    Ces deux notions sont indépendantes l'une de l'autre.  
    Les protocoles orienté connexion sont généralement fiables, mais pas obligatoirement. Et réciproquement.

!!! exp "Fiable vs Orienté Connexion"
    * les protocoles HTTP, TCP sont Orientés Connexion et Fiables
    * le protocole FR - [Frame Relay](https://fr.wikipedia.org/wiki/Relais_de_trames) est
        * non fiable
        * orienté connexion
    * 


## Interfaces

!!! def "Interfaces"
    Une <bred>interface</bred> (réseau) est la matérialisation de l'intermédiaire entre l'ordinateur et le canal de communication. Une **interface réseau** est en pratique:

    * une <bred>carte réseau</bred> ou <bred>NIC - Network Interface Card</bred>
    * un port soudé directement à la carte mère
    * un des ports de sortie dans un Hub, un Switch ou un Routeur
