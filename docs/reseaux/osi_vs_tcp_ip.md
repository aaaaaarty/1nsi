# 1NSI: Réseaux: OSI vs TCP/IP

## Schéma de Comparaison

![OS vs TCP IP](../img/OSI_vs_TCP_IP.png)

## Adressage, identifiants et matériels

Les machines et leurs interfaces disposent d’identifiants au niveau de chaque couche.

|Couche|Identifiant|Exemple|
|:-:|:-:|:-:|
|Couche Application|Un protocole et un nom de domaine|`http://` suivi de www.cisco.com|
|Couche Transport|	Port TCP ou UDP|TCP80 comme port par défaut pour HTTP|
|Couche Internet|	Adresse IPv4 et/ou IPv6|192.168.130.252/24 ou 2001:ae6::1/64|
|Couche Accès|	adresse physique (MAC 802)|70:52:63:dc:8e:47|

## Résumé en Tableau

|OSI|TCP/IP|Rôles|PDU|Protocoles|Matériel|
|:-:|:-:|:-:|:-:|:-:|:-:|
|7 Application|Application|Services au plus proche des utilisateurs|Données|HTTP, DNS, DHCP|Ordinateurs|
|6 Présentation|Application|encode, chiffre, compresse les données utiles|idem| idem| idem|
|5 Session|Application|établit des sessions entre des applications|idem|idem|idem|
|4 Transport|Transport|établit, maintient, termine des sessions entre des hôtes d’extrémité.|Segment|TCP ou UDP|Ordinateurs, routeurs NAT, pare-feux|
|3 Réseau|Internet|Identifie les hôtes et assure leur connectivité|Datagramme ou paquet|IPv4 ou IPv6|Routeurs|
|2 Liaison de Données|Accès Réseau|Détermine la méthode d’accès au support, organise les bits de données|Trame|Ethernet IEEE 802.3, Wi-Fi IEEE 802.11, pontage 802.1|Commutateurs, cartes d’interface réseau|
|1 Physique|Accès Réseau|s’occupe du placement physique du signal|bits|Normes physiques : xDSL (WAN), 1000-BASE-TX|Câblage (UTP CAT 6) et connecteurs (RJ-45), bande fréquences (2.4 GHz, 5 GHz)|
