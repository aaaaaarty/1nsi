# 1NSI: Réseaux

Un <bred>réseau</bred> :fr: ou <bred>network</bred> :gb:, est un ensemble d'éléments, **matériels** et **logiciels**, permettant le **partage d'informations et de ressources**, via des **canaux de communications**.

Il est classiquement composé de:

* d'**éléments matériels**, appelés <bred>matériels réseau</bred>, reliés entre eux
* d'**éléments logiciels**: drivers, dirmwares, applications réseaux, etc..