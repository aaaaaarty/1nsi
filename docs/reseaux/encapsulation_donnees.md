# 1NSI: Réseaux: Encapsulations des Données

## Encapsulation et Décapsulation 

!!! def "Encapsulation vs Décapsulation"
    1. Lorsque les données passent d'une couche supérieure (plus proche de l'utilisateur) à une couche directement inférieure (plus proche de la machine), elles sont complétées par :
        * un <bred>en-tête</bred> :fr: (ou <bred>entête</bred>) ou <bred>Header</bred> :gb: spécifique à la couche traversée,
        * Quelquefois également, selon les couches et les protocoles, d'un <bred>pied</bred> ou <bred>footer</bred>  
        C'est ce que l'on appelle l'<bred>Encapsulation</bred> des données  
    2. **À l'opposé**, lorsque les données passent d'une couche inférieure (plus proche de la machine) à une couche directement supérieure (plus proche de l'utilisateur), elles sont démunies de l'**entête** / **header** (et du **pied**/**footer**): c'est ce que l'on appelle la <bred>Décapsulation</bred> des données.

![Encapsulation](../img/encapsulation.svg){.center}

## PDU - Protocol Data Unit

!!! def "PDU - Protocol Data Unit"

## Charge Utile

!!! pte "Charge Utile"
    Un PDU est composé principalement de 2 (éventuellement 3) parties:

    * Un **entête**
    * une <bred>Charge Utile</bred> ou <bred>Payload</bred> :gb: : c'est la partie qui contient les données utiles/brutes, sans autres fioritures
    * un **footer** (éventuellement)

## Segments vs Datagrammes vs Paquets vs Trames

Nous avons vu que le terme PDU est un mot générique (valable pour toutes les couches). Néanmoins, de manière plus précise, chaque couche spécifique dispose de son propre vocabulaire/terme pour désigner le nom du PDU correspondant:

* Pour le niveau $4$ (couche **Transport**), un PDU s'appelle un <bred>Segment</red> (TCP) de données :fr: :gb: ou <bred>Datagramme</bred> / <bred>Datagram :gb:</bred>
* Pour le niveau $3$ (couche **Réseau**), un PDU s'appelle un <bred>Paquet</red> :fr: ou <bred>packet</bred> :gb:
* Pour le niveau $2$ (couche **Liaison de Données**), un PDU s'appelle une <bred>trame</red> :fr: ou <bred>frame</bred> :gb:
* Pour le niveau $1$ (couche **Physique**), un PDU n'existe plus vraiment, car la charge utile est un bit ... (il n'y a plus d'encapsulation..), mais on considère quelquefois que l'équivalent du PDU est un bit;

<env>Résumé</env>

<center>

| Niveau | Nom de la Couche | Nom du PDU |
|:-:|:-:|:-:|
| $4$ | Transport | Segment (TCP) ou Datagramme (UDP) |
| $3$ | Réseau | Paquet (=Packet) |
| $2$ | Liaison de Données | Trame (=Frame)|
| $1$ | Physique | Bit |

</center>

![OSI PDU par Couche](../img/osi-pdu-par-couche.png)

