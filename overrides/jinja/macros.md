{% macro input(name, value='', type='text', size=20) -%}
    <input type="{{ type }}" name="{{ name }}" value="{{
        value|e }}" size="{{ size }}">
{%- endmacro %}

{% macro year() -%}
    <span class="year"></span>
    <script>
      var year = new Date().getFullYear();
      var span = document.getElementsByClassName("year");
      console.log(span);
      span[0].innerHTML = year;
    </script>
{%- endmacro %}