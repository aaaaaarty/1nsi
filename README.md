Bienvenue sur la page d'accueil du **Dépôt de 1NSI**, du **Projet Eskool : Le Libre pour l'Education.**

Si vous êtes un élève, vous cherchez plutôt (probablement) **le site eskool 1NSI** qui est accessible sur la page :  
https://eskool.gitlab.io/1nsi/
